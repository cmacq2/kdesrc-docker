# Building KDE using kdesrc-build with Docker

## About
The aim of this project is to explore building KDE software using `kdesrc-build` in Docker containers.
The main objectives are:

 - Minimising system requirements to build KDE software
 - Keeping the docker containers as 'pure' build environments, that is: keeping source, destination and build configuration (`kdesrc-build.rc`) outside the container images.
 - Ensuring that files generated during build are immedately useful outside the docker containers used to build
 - Remaining easy to use

## Limitations
Currently the project supports Debian Sid (unstable) only. Fixing this is a matter of adding (many?) more Dockerfiles to `./docker` to choose from when running `kdesrc-build`.

## Requirements

 - `docker`
 - Currently only docker images for Debian Sid (unstable) are available

On Debian based systems you may need to add yourself to the `docker` user group in order to be able to use Docker:

```
    sudo adduser $(whoami) docker
```

In that case, you need to log out and log back in for the change to take effect.

## Getting started & general usage
Pick a working directory & clone this repository to it:

```
    WORK_DIR="$HOME/projects/kde"

    mkdir -p "$WORK_DIR"
    git clone https://gitlab.com/cmacq2/kdesrc-docker.git "$WORK_DIR"
```

Run the `./scripts/setup.sh` script to setup `kdesrc-build` for you:

```
    cd "$WORK_DIR"
    ./scripts/setup.sh
```

Review the contents of the generated `kdesrc-build.rc` file with your favourite text editor:

```
    kate -b "$WORK_DIR/kdesrc-build.rc"
```

Fix any items marked `TODO`, save & close your text editor.

Next, examine the available docker images:

```
    cd "$WORK_DIR"
    ./scripts/kdesrc-kdesrc-build.sh --list
```

With the exception of the 'base' Docker images, all listed names refer to KDE projects you can build using the corresponding image.
Finally building a project, e.g. `kirigami`, using a particular docker image is as easy as:

```
    cd "$WORK_DIR"
    ./scripts/kdesrc-build.sh sid-slim kirigami
```

Afterwards you can find the source code of the project that was built in `$WORK_DIR/src`, and any installed files such as built libraries or binaries in `$WORK_DIR/dist`.

You can pass additional options to `kdesrc-build` by passing them as subsequent arguments to `kdesrc-build.sh`.
For example, downloading or updating the KDE source repositories all in one go:

```
    cd "$WORK_DIR"
    ./scripts/kdesrc-build.sh sid-slim --no-build --no-install
```

For more information on available `kdesrc-build` commandline arguments consult [its documentation](https://kdesrc-build.kde.org/), or ask it for `--help`:

```
    cd "$WORK_DIR"
    ./scripts/kdesrc-build.sh sid-slim --help
```

If you need to purge your KDE source or installation directories you can use `./scripts/cleanup.sh`.
To force an update of the docker containers, you can remove the corresponding images using the `docker` tool.

## Uploading your patches for review
In addition to `kdesrc-build` this project also provides a convenient Docker container for working with `arcanist`.
You can use `arcanist` to upload your patches for review to KDE's Phabricator instance.
To get started on improving KDE with `kdesrc-build` and `arcanist`, pick your project (e.g. `plasma-desktop`):

```
    PROJECT="$WORK_DIR/src/kde/workspace/plasma-desktop
    cd "$PROJECT"
```

Next, create your own branch:

```
    cd "$PROJECT"
    git checkout -b improvements
```

Edit the project's sources and then use `kdesrc-build` to build the project with your improvements:

```
    cd "$WORK_DIR"
    ./scripts/kdesrc-build.sh sid-slim --no-src
```

Test and if everything is OK submit your patches to KDE's Phabricator using for example:

```
    cd "$PROJECT"
    "$WORK_DIR/scripts/arcanist" sid-slim
```

For more information on available `arcanist` commandline arguments ask it for `--help`:

```
    cd "$WORK_DIR"
    ./scripts/arcanist.sh sid-slim --help
```

## Debugging
In addition to the normal workflow, a few special modes are available to aid debugging.
To explore the build environment from inside the container you can enter a shell (`/bin/sh`) instead with:

```
    cd "$WORK_DIR"
    ./scripts/kdesrc-build.sh --shell sid-slim
```

Additional arguments are passed to the container as before, so to dump the environment variables you can do:

```
    cd "$WORK_DIR"
    ./scripts/kdesrc-build.sh --shell sid-slim -c export
```

If you are debugging the Dockerfile of a particular container variant, you may find `--build-only` useful to exit after building a container image:

```
    cd "$WORK_DIR"
    ./scripts/kdesrc-build.sh --build-only sid-slim
```

These debugging features are also available for the `arcanist` container through `./scripts/arcanist.sh`.

#!/bin/sh

#
# Sets up the development environment
#

set -e

SCRIPT=$(readlink -m "$0")
SCRIPT_DIR=$(dirname "$SCRIPT")

cd "$SCRIPT_DIR/.."

ROOT="$(pwd)"

KDE_SOURCE_DIR="$ROOT/src"
KDE_DIST_DIR="$ROOT/dist"
QT_DIST_DIR="$KDE_DIST_DIR/custom/qt"

KDESRC_BUILD_DIR="$ROOT/kdesrc-build"
KDESRC_BUILD_RC="$ROOT/kdesrc-build.rc"
ARC_RC="$ROOT/arc.rc"
GIT_CONFIG="$ROOT/git.conf"
SSH_CONFIG="$ROOT/ssh.conf"
SSH_HOSTS="$ROOT/ssh_hosts.conf"

mkdir -p "$KDE_SOURCE_DIR"
mkdir -p "$KDE_DIST_DIR"

if which git >/dev/null
then
    cores="$(nproc --all)"
    jobs="$(($cores * 5 / 4))"
    user=""
    email=""
    warn=""
    user=$(git config user.name) || user=$(git config --global user.name) || warn=" #TODO: edit this"
    email=$(git config user.email) || email=$(git config --global user.email) || warn=" #TODO: edit this"
    if [ -z "$user" ]
    then
        user="Your Name"
    fi
    if [ -z "$email" ]
    then
        email="<you@example.com>"
    fi

    if [ ! -d "$ROOT/kdesrc-build" ]
    then
        echo "Cloning kdesrc-build to: $KDESRC_BUILD_DIR"
        echo
        git clone https://anongit.kde.org/kdesrc-build "$KDESRC_BUILD_DIR"
    fi

    if [ ! -f "$GIT_CONFIG" ]
    then
        echo
        echo "Generating .gitconfig file: $GIT_CONFIG"
        echo
        tee "$GIT_CONFIG" << GIT
[user]$warn
   name = $user
   email = $email

[url "https://anongit.kde.org/"]
   insteadOf = kde:
[url "ssh://git@git.kde.org/"]
   pushInsteadOf = kde:
GIT
        echo
        echo "Please review the generated file at: $GIT_CONFIG"
        echo "In particular, review & fix any item marked TODO"
        echo
    fi

    if [ ! -f "$ARC_RC" ]
    then
        echo
        echo "Generating dummy arc.rc file: $ARC_RC"
        echo
        echo "{}" > "$ARC_RC"
        chmod 600 "$ARC_RC"
    fi

    if [ ! -f "$SSH_HOSTS" ]
    then
        echo
        echo "Generating dummy .ssh/known_hosts file: $SSH_HOSTS"
        echo
        touch "$SSH_HOSTS"
    fi

    if [ ! -f "$SSH_CONFIG" ]
    then
        echo
        echo "Generating .ssh/config file: $SSH_CONFIG"
        echo
        tee "$SSH_CONFIG" << SSH
Host *.kde.org
        User git
        IdentityFile ~/.ssh/kde_identity
SSH
        echo
        echo "Please review generated file at: $SSH_CONFIG"
        echo
    fi

    if [ ! -f "$KDESRC_BUILD_RC" ]
    then
        echo
        echo "Generating kdesrc-build.rc file: $KDESRC_BUILD_RC"
        echo
        tee "$KDESRC_BUILD_RC" << CFG

# This is a sample kdesrc-build configuration file appropriate for KDE
# Frameworks 5-based build environments.
#
# See the kdesrc-buildrc-sample for explanations of what the options do, or
# view the manpage or kdesrc-build documentation at
# https://docs.kde.org/trunk5/en/extragear-utils/kdesrc-build/index.html
global
    git-user $user <$email>$warn
    branch-group kf5-qt5

    # Where to download source code. By default the build directory and
    # logs will be kept under this directory as well.
    source-dir $KDE_SOURCE_DIR
    kdedir $KDE_DIST_DIR
    qtdir $QT_DIST_DIR

    cmake-options -DCMAKE_BUILD_TYPE=RelWithDebInfo
    cmake-generator Ninja
    make-options -j$jobs
end global

# Instead of specifying modules here, the current best practice is to refer to
# KF5 module lists maintained with kdesrc-build by the KF5 developers. As new
# modules are added or modified, the kdesrc-build KF5 module list is altered to
# suit, and when you update kdesrc-build you will automatically pick up the
# needed changes.

# NOTE: You MUST change the path below to include the actual path to your
# kdesrc-build installation.
# Inside the docker container(s) this will be at /kdesrc-build/bin
# Do not change if unsure
include /kdesrc-build/bin/kf5-qt5-build-include
include /kdesrc-build/bin/qt5-build-include

# If you wish to maintain the module list yourself that is possible, simply
# look at the files pointed to above and use the "module-set" declarations that
# they use, with your own changes.

# It is possible to change the options for modules loaded from the file
# included above (since it's not possible to add a module that's already been
# included), e.g.

options gpgme
    # work around: add libgpg-error include path manually
    set-env CFLAGS -I$KDE_DIST_DIR/include
end options

options qt5-set
    make-options -j$jobs NINJAFLAGS=-j$jobs
end options
CFG
        echo
        echo "Please review the generated file at: $KDESRC_BUILD_RC"
        echo "In particular, review & fix any item marked TODO"
        echo
    fi

    echo "Setup completed successfully"
else
    echo "Git is not installed? Unable to continue." >&2
    exit 1
fi

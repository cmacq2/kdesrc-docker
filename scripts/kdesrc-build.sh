#!/bin/sh
#
# Launcher script for invoking kdesrc-build from inside its container.
#

TOOL_NAME="kdesrc-build"
IMAGE_NAME="kdesrc-build"

custom_mounts ()
{
    echo -n " -v \"$1/kdesrc-build:/kdesrc-build/bin:ro\""
    echo -n " -v \"$1/kdesrc-build.rc:$HOME/.kdesrc-buildrc:ro\""
    echo -n " -v \"$1/git.conf:$HOME/.gitconfig:ro\""
}

. "$(dirname "$(readlink -m "$0")")/docker.sh"

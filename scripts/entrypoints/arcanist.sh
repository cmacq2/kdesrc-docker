#!/bin/sh
#
# A wrapper entrypoint script for docker containers:
# its main purpose is to ensure the environment is sane before invoking arcanist.
#

set -e

error ()
{
    echo
    cat << ERROR
Cowardly refusing to run arcanist
$1
This container is meant to be run from somewhere inside: $SOURCE_DIR

ERROR
}

if [ ! -d "$WORK_DIR" ]
then
    error "Your working directory does not appear to exist inside the container: $WORK_DIR" >&2
    exit 255
fi

set -x

cd "$WORK_DIR"

exec arc "$@"

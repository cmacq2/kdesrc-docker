#!/bin/sh

set -e

SCRIPT=$(readlink -m "$0")
SCRIPT_DIR=$(dirname "$SCRIPT")

cd "$SCRIPT_DIR/.."

ROOT="$(pwd)"

KDE_SOURCE_DIR="$ROOT/src"
KDE_DIST_DIR="$ROOT/dist"

rm -rf "$KDE_SOURCE_DIR"
rm -rf "$KDE_DIST_DIR"

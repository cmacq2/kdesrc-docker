#!/bin/sh
#
# Launcher script for invoking arcanist from inside its docker container.
#

TOOL_NAME="arcanist"
IMAGE_NAME="arcanist"

custom_mounts ()
{
    echo -n " -v \"$1/arc.rc:$HOME/.arcrc\""
    echo -n " -v \"$1/ssh_hosts.conf:$HOME/.ssh/known_hosts\""
    echo -n " -v \"$1/scripts/entrypoints/arcanist.sh:/arcanist/wrapper.sh:ro\""
    echo -n " -v \"$1/kde_identity:$HOME/.ssh/kde_identity:ro\""
    echo -n " -v \"$1/ssh.conf:$HOME/.ssh/config:ro\""
    echo -n " -v \"$1/git.conf:$HOME/.gitconfig:ro\""
}

. "$(dirname "$(readlink -m "$0")")/docker.sh"

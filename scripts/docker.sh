#!/bin/sh
#
# Common/shared launcher script for docker operations.
#

set -e

WORK_DIR="$(pwd)"

SCRIPT_NAME="$(basename "$0")"
SCRIPT_DIR="$(dirname "$(readlink -m "$0")")"

ROOT="$(readlink -m "$SCRIPT_DIR/..")"

VARIANT_NAME=""
MEMORY_LIMIT="24g"

SOURCE_DIR="$ROOT/src"
KDESRC_BUILD_DIR="$ROOT/kdesrc-build"
KDE_DIR="$ROOT/dist"
MODE="default"
ENTRYPOINT_OVERRIDE=""

sanity_check ()
{
    local value="$(eval echo -n "\"\$$1\"")"
    if [ -z "$value" ]
    then
        echo "Sanity check failed, frontend ($SCRIPT_NAME) did not set: $1" >&2
        exit 255
    fi
}

display_usage ()
{
        echo
        cat << USAGE
$SCRIPT_NAME builds and runs $TOOL_NAME docker images.

Usage: $SCRIPT_NAME <variant> [$TOOL_NAME args...]

Options:

 -h, --help                : display this 'help' instead
 -l, -ls, --list           : display a list of available image variants instead
 -s, -sh, --shell          : enter a shell (/bin/sh) inside the container instead
 -b, --build, --build-only : build the container and exit

USAGE
}

pick_variant ()
{
    if [ -z "$1" ]
    then
        echo "A valid variant name is required" >&2
        display_usage >&2
        exit 1
    elif [ -f "$ROOT/docker/$TOOL_NAME/$1.docker" ]
    then
        VARIANT_NAME="$1"
    else
        echo "Not a valid variant name: '$1'" >&2
        display_usage >&2
        exit 1
    fi
}

get_run_command()
{
    #
    # Generate the minimal/base command to run the build inside the container
    #
    #  - as the user that runs this script; so generated output will be available to the user without requiring sudo/su
    #  - mapping a fixed source directory
    #  - mapping a fixed destination/dist directory to 'make install' to
    #  - set up environment variables for Qt tools
    #  - pass arguments to this script on: this lets the user override CMD defaults from the docker image directly.
    #    i.e. it lets the user pass options to kdesrc-build running inside the container.
    #
    echo -n docker run -it --rm \
    --memory-swap "$MEMORY_LIMIT" --memory "$MEMORY_LIMIT" \
    -u "$(id -u):$(id -g)" -v "/etc/passwd:/etc/passwd:ro" \
    -v "\"$SOURCE_DIR:$SOURCE_DIR:rw\"" \
    -v "\"$KDE_DIR:$KDE_DIR:rw\"" \
    $TOOL_VOLUME_MOUNTS \
    -e "XDG_DATA_DIRS=\"$DATA_DIRS\"" -e QT_SELECT=5 \
    -e "WORK_DIR=\"$WORK_DIR\"" \
    $ENTRYPOINT_OVERRIDE \
    "\"$IMAGE_NAME:$VARIANT_NAME\""
}

sanity_check TOOL_NAME
sanity_check IMAGE_NAME

TOOL_VOLUME_MOUNTS="$(custom_mounts "$ROOT" 2>/dev/null || echo -n "")"
sanity_check TOOL_VOLUME_MOUNTS

case "$1" in
    -h|--help)
        display_usage
        exit 0
    ;;
    -l|-ls|--list)
        cd "$ROOT/docker"
        for v in "$TOOL_NAME/"*.docker
        do
            if [ -f "$v" ]
            then
                echo "$(basename "$v" .docker)"
            fi
        done
        exit 0
    ;;
    -b|--build|--build-only)
        MODE="build-only"
        shift
        pick_variant "$1"
        shift # for consistency with the other invocations
    ;;
    -s|-sh|--shell)
        MODE="shell"
        ENTRYPOINT_OVERRIDE="--entrypoint /bin/sh"
        shift
        pick_variant "$1"
        shift # ensure /bin/sh won't see a bogus first argument
    ;;
    *)
        pick_variant "$1"
        shift # ensure $tool won't see a bogus first argument
    ;;
esac

# inject $KDE_DIR/share into the default data search path. Things like QStandardPaths should pick up on this.
DATA_DIRS="$KDE_DIR/share"
if [ -n "$XDG_DATA_DIRS" ]
then
    DATA_DIRS="$DATA_DIRS:$XDG_DATA_DIRS"
fi

# some tools like qmlplugindump complain if XDG_RUNTIME_DIR is not set or does not point to a valid (?) directory
# which is why the containers include a dummy directory for it in the image.
if [ -z "$XDG_RUNTIME_DIR" ]
then
    XDG_RUNTIME_DIR="/run/user/$(id -u)"
fi

RUN_COMMAND="$(get_run_command)" # do this before set -x to avoid confusing/redundant output

set -x

docker build --rm \
    --build-arg USER=$(id -u) \
    --build-arg GROUP=$(id -g) \
    --build-arg KDE_DIR="$KDE_DIR" \
    --build-arg HOME_DIR="$HOME" \
    --build-arg SOURCE_DIR="$SOURCE_DIR" \
    --build-arg XDG_RUNTIME_DIR="$XDG_RUNTIME_DIR" \
    -t "$IMAGE_NAME:$VARIANT_NAME" \
    - < "$ROOT/docker/$TOOL_NAME/$VARIANT_NAME.docker"

if [ "$MODE" = "build-only" ]
then
    exit 0
fi

#
# Make sure to 'regenerate' these directories if deleted as part of cleanup.
#
mkdir -p "$SOURCE_DIR"
mkdir -p "$KDE_DIR"
mkdir -p "$KDESRC_BUILD_DIR"

# Run the container
# Quoting things properly is hard... the main difficulty comes from spaces in paths.
eval "$RUN_COMMAND" "$@"
